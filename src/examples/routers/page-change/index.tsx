import { BrowserRouter, Link, Route } from 'react-router-dom';

function PageChangeHome() {
  return <div>Home Page!</div>;
}
function PageChangeLogin() {
  return <div>Login Page!</div>;
}

export default function PageChange(props: any) {
  return (
    <div>
      <BrowserRouter>
        <Link to="/">Home</Link> | <Link to="/login">Login</Link>
        <Route path="/" exact component={PageChangeHome} />
        <Route path="/login" exact component={PageChangeLogin} />
      </BrowserRouter>
    </div>
  );
}
