import useStyles from './index.style';

interface ButtonProps {
  title: string;
  onClick?: () => void;
  type?: 'submit' | 'reset' | 'button';
}

export default function Button(props: ButtonProps) {
  const classes = useStyles();
  return (
    <button
      type={props.type ?? 'button'}
      onClick={() => props.onClick && props.onClick()}
      className={classes.root}
    >
      {props.title}
    </button>
  );
}
