import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  root: {
    fontSize: '16px',
    color: 'black',
    textAlign: 'center',
    border: '1px solid #6C8EBF',
    backgroundColor: '#DAE8FC',
    minWidth: '120px',
    minHeight: '40px',
    lineHeight: '38px',
    borderRadius: '5px',
    cursor: 'pointer',

    '&:hover': {
      backgroundColor: '#B3D0FC',
    },
    '&:active': {
      backgroundColor: '#9EB7DE',
    },
  },
});

export default useStyles;
