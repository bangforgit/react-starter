import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Main from '.';

export default {
  title: 'Examples/components/Button',
  component: Main
} as ComponentMeta<typeof Main>;

const Template: ComponentStory<typeof Main> = args => <Main {...args} />;

export const Default = Template.bind({});
Default.args = {
  title: '按鈕',
  onClick: () => {
    console.log('Clicked!');
  }
};
