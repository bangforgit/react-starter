import PageChange from './examples/routers/page-change';

function App() {
  return (
    <div className="App">
      <PageChange />
    </div>
  );
}

export default App;
